// Shader-ul de varfuri  
 
 #version 400


layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Color;
layout(location=2) in vec3 in_Normal;
 

out vec4 gl_Position; 
out vec3 Normal;
out vec3 FragPos;
out vec4 ex_Color;
 
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
 


void main(void)
  {
    gl_Position = projection*view*model*in_Position;
    Normal=mat3(transpose(inverse(model))) *in_Normal; 
	FragPos = vec3(model * in_Position);
	ex_Color=in_Color;
   } 
 