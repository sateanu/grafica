﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    class Horn : Actor
    {
        Func<float, float> func = (x) => (float)1/x;
        Vector2 dom = new Vector2(0f, 800);
        float curvePoints = 6000;
        float rotPoints = 100;
        public Horn(World world) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, new Vector3(1, 0.5f, 0),
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }
        public Horn(World world, Vector3 color) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, color,
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }
        public override void Draw()
        {
            base.Draw(OpenTK.Graphics.OpenGL4.PrimitiveType.TriangleStrip, Vertices.Count() / 3);
        }
    }
}
