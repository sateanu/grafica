﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    class Square : Actor
    {
        public static float[] SquareVertices =
        {
            -1,1,0,
             1,1,0,
            -1,-1,0,
             1,-1,0
        };

        public static float[] SquareColors =
        {
             1,0,0,
             0,1,0,
             0,0,1,
             1,1,0
        };

        public Square(World world) : base(world)
        {
            Vertices = SquareVertices;
            Colors = SquareColors;
        }

        public Square(World world, Vector3 color) : base(world)
        {
            Vertices = SquareVertices;
            Colors = new float[]
            {
                color.X,color.Y,color.Z,
                color.X,color.Y,color.Z,
                color.X,color.Y,color.Z,
                color.X,color.Y,color.Z,
            };
        }

        public override void Draw()
        {
            this.Draw(OpenTK.Graphics.OpenGL4.PrimitiveType.TriangleStrip, 4);
        }
    }
}
