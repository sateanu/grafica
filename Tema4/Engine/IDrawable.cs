﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;
using OpenTK.Graphics.OpenGL4;

namespace Tema4.Engine
{
    interface IDrawable : IObject
    {
        Color Color { get; set; }

        void Draw(PrimitiveType type, int count);
    }
}
