﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
namespace Tema4.Engine
{
    public class Camera
    {
        public Vector3 Position;

        public Vector3 Rotation;

        public Matrix4 Projection { get; set; }

        public Matrix4 RotationMatrix
        {
            get
            {
                Matrix4 camRot = Matrix4.CreateRotationX(Rotation.X) * Matrix4.CreateRotationY(Rotation.Y) * Matrix4.CreateRotationZ(Rotation.Z);
                return camRot;
            }
        }

        public Matrix4 View
        {
            get
            {
                Matrix4 camRot = RotationMatrix;
                Vector3 cameraOriginalTarget = new Vector3(0, 0, -1.0f);
                Vector3 cameraRotatedTarget = Vector3.Transform(cameraOriginalTarget, camRot);
                Vector3 cameraFinalTarget = Position + cameraRotatedTarget;

                Vector3 cameraUpVector = new Vector3(0, 1, 0);
                Vector3 cameraRotatedUpVector = Vector3.Transform(cameraUpVector, camRot);

                Matrix4 view = Matrix4.LookAt(Position, cameraFinalTarget, cameraRotatedUpVector);

                return view;
            }
        }

        public Camera()
        {
            Position = Vector3.Zero; 
            Rotation = Vector3.Zero;
        }

        internal void AddPosition(Vector3 moveVector)
        {
            Vector3 rotatedVector = Vector3.Transform(moveVector, RotationMatrix);
            Position += rotatedVector;
        }
    }
}