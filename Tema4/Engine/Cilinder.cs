﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    class Cilinder : Actor
    {
        Func<float, float> func = (x) =>
        {
            float y=1;
            if (x == 0)
                y = 0;
            if (x == 1)
                y = 0;

            return y;
        };
        Vector2 dom = new Vector2(0, 1);
        float curvePoints=1000;
        float rotPoints=300;
        public Cilinder(World world) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, new Vector3(1, 0.5f, 0),
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }

        public Cilinder(World world, Vector3 color) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, color,
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }

        public override void Draw()
        {
            base.Draw(OpenTK.Graphics.OpenGL4.PrimitiveType.TriangleStrip, Vertices.Count()/3);
        }
    }
}
