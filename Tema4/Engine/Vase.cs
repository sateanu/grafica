﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    class Vase : Actor
    {
        Func<float, float> func = (x) => (float)Math.Sin(x)+1;
        Vector2 dom = new Vector2(0, (float)Math.PI*2);
        float curvePoints = 600;
        float rotPoints = 100;
        public Vase(World world) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, new Vector3(1, 0.5f, 0),
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }
        public Vase(World world, Vector3 color) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, color,
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }
        public override void Draw()
        {
            base.Draw(OpenTK.Graphics.OpenGL4.PrimitiveType.TriangleStrip, Vertices.Count()/3);
        }
    }
}
