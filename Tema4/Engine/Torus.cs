﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    class Torus : Actor
    {
        Func<float, float> func = (x) =>
        {
            return (float)Math.Sqrt(1 - (x) * (x));
        };
        Vector2 dom = new Vector2(-1, 1);
        float curvePoints = 60;
        float rotPoints = 60;
        public Torus(World world) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, new Vector3(1, 0.5f, 0),
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }
        public Torus(World world, Vector3 color) : base(world)
        {
            RevolutionSurface surf = new RevolutionSurface(curvePoints, rotPoints, color,
                Vector3.UnitZ, dom, func);

            surf.Generate();

            Vertices = surf.vertices.ToFloatArray();
            Colors = surf.colors.ToFloatArray();
            Normals = surf.normals.ToFloatArray();
        }
        public override void Draw()
        {
            base.Draw(OpenTK.Graphics.OpenGL4.PrimitiveType.Lines, Vertices.Count() / 3);
        }
    }
}
