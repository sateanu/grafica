﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    public static class Extensions
    {
        public static float[] ToFloatArray(this List<Vector3> list)
        {
            float[] array = new float[list.Count * 3];

            for (int i = 0; i < list.Count; i++)
            {
                array[i*3] = list[i].X;
                array[i*3+1] = list[i].Y;
                array[i*3+2] = list[i].Z;
            }

            return array;
        }
    }
}
