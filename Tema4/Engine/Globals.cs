﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    public static class Globals
    {
        public static int ProgramId;

        public static Vector3 lightColor = new Vector3(1, 1, 1);
        public static Vector3 ambientColor = new Vector3(0.1f, 0.1f, 0.1f);
        public static Vector3 lightPos = new Vector3(50, 50, 50);
    }
}
