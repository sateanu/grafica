﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Tema4.Engine
{
    public class Actor : IDrawable, IUpdatable
    {
        public Color Color { get; set; }

        public Vector3 Position { get; set; }

        public Vector3 Rotation { get; set; }

        public Vector3 Scale { get; set; }

        public float[] Vertices
        {
            get
            {
                return vertices;
            }

            set
            {
                vertices = value;
            }
        }

        public float[] Normals
        {
            get
            {
                return normals;
            }

            set
            {
                normals = value;
            }
        }

        public float[] Colors
        {
            get
            {
                return colors;
            }

            set
            {
                colors = value;
            }
        }

        public List<IObject> Children
        {
            get;
            set;
        }

        private float[] vertices;

        private float[] normals;

        private float[] colors;

        private int VaoId;

        private World world;

        public Actor(World world)
        {
            this.world = world;
            Position = Vector3.Zero;
            Rotation = Vector3.Zero;
            Scale = Vector3.One;
        }

        public Actor(World world, float[] vertices)
        {
            this.world = world;
            Position = Vector3.Zero;
            Rotation = Vector3.Zero;
            Scale = Vector3.One;
            this.vertices = vertices;
        }

        public void Initialize()
        {
            int attribIndex = 0;
            VaoId = GL.GenVertexArray();
            GL.BindVertexArray(VaoId);
            int vertexVbo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexVbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);
            GL.EnableVertexAttribArray(attribIndex);
            GL.VertexAttribPointer(attribIndex,3, VertexAttribPointerType.Float, false, 0, 0);
            attribIndex++;
            if (colors != null && colors.Length > 0)
            {
                int colorVbo = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, colorVbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(colors.Length * sizeof(float)), colors, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(attribIndex);
                GL.VertexAttribPointer(attribIndex, 3, VertexAttribPointerType.Float, false, 0, 0);
                attribIndex++;
            }
            if (normals != null && normals.Length > 0)
            {
                int normalVbo = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, normalVbo);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(attribIndex);
                GL.VertexAttribPointer(attribIndex, 3, VertexAttribPointerType.Float, false, 0, 0);
                attribIndex++;
            }

        }

        public virtual void Draw(PrimitiveType type, int count)
        {
            int modelMatLocation = GL.GetUniformLocation(Globals.ProgramId, "model");
            int viewMatLocation = GL.GetUniformLocation(Globals.ProgramId, "view");
            int projMatLocation = GL.GetUniformLocation(Globals.ProgramId, "projection");
            int lightColorLocation = GL.GetUniformLocation(Globals.ProgramId, "lightColor");
            int lightPosLocation = GL.GetUniformLocation(Globals.ProgramId, "lightPos");
            int viewPosLocation = GL.GetUniformLocation(Globals.ProgramId, "viewPos");
            int ambientalColorLocation = GL.GetUniformLocation(Globals.ProgramId, "ambientalColor");

            Matrix4 proj = world.Camera.Projection;
            Matrix4 view = world.Camera.View;
            Matrix4 rot = Matrix4.CreateRotationX(Rotation.X) *
                Matrix4.CreateRotationY(Rotation.Y) *
                Matrix4.CreateRotationZ(Rotation.Z);
            
            Matrix4 model =   Matrix4.CreateScale(Scale) * rot * Matrix4.CreateTranslation(Position);

            GL.UniformMatrix4(projMatLocation, false, ref proj);
            GL.UniformMatrix4(viewMatLocation, false, ref view);
            GL.UniformMatrix4(modelMatLocation, false, ref model);
            GL.Uniform3(lightPosLocation, Globals.lightPos);
            GL.Uniform3(lightColorLocation, Globals.lightColor);
            GL.Uniform3(ambientalColorLocation, Globals.ambientColor);
            GL.Uniform3(viewPosLocation, world.Camera.Position);

            GL.BindVertexArray(VaoId);
            GL.DrawArrays(type, 0, count);
        }
        public virtual void Draw() { }
    }
}
