﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
namespace Tema4.Engine
{
    public class World
    {
        public Camera Camera{ get; set; }

        public World()
        {
            Camera = new Camera();   
        }
    }
}