﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    class Cube : Actor
    {
        static float[] CubeVertices =
            {
                -1.0f, -1.0f, -1.0f,
                 1.0f, -1.0f, -1.0f,
                 1.0f,  1.0f, -1.0f,
                 1.0f,  1.0f, -1.0f,
                -1.0f,  1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,

                -1.0f, -1.0f,  1.0f,
                 1.0f, -1.0f,  1.0f,
                 1.0f,  1.0f,  1.0f,
                 1.0f,  1.0f,  1.0f,
                -1.0f,  1.0f,  1.0f,
                -1.0f, -1.0f,  1.0f,

                -1.0f,  1.0f,  1.0f,
                -1.0f,  1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f,  1.0f,
                -1.0f,  1.0f,  1.0f,

                 1.0f,  1.0f,  1.0f,
                 1.0f,  1.0f, -1.0f,
                 1.0f, -1.0f, -1.0f,
                 1.0f, -1.0f, -1.0f,
                 1.0f, -1.0f,  1.0f,
                 1.0f,  1.0f,  1.0f,

                -1.0f, -1.0f, -1.0f,
                 1.0f, -1.0f, -1.0f,
                 1.0f, -1.0f,  1.0f,
                 1.0f, -1.0f,  1.0f,
                -1.0f, -1.0f,  1.0f,
                -1.0f, -1.0f, -1.0f,

                -1.0f,  1.0f, -1.0f,
                 1.0f,  1.0f, -1.0f,
                 1.0f,  1.0f,  1.0f,
                 1.0f,  1.0f,  1.0f,
                -1.0f,  1.0f,  1.0f,
                -1.0f,  1.0f, -1.0f,
            };

        static float[] CubeColors =
            {
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,

                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,

                0.0f,1.0f,1.0f,
                0.0f,1.0f,1.0f,
                0.0f,1.0f,1.0f,
                0.0f,1.0f,1.0f,
                0.0f,1.0f,1.0f,
                0.0f,1.0f,1.0f,

                1.0f,1.0f,1.0f,
                1.0f,1.0f,1.0f,
                1.0f,1.0f,1.0f,
                1.0f,1.0f,1.0f,
                1.0f,1.0f,1.0f,
                1.0f,1.0f,1.0f,

                1.0f,0.0f,1.0f,
                1.0f,0.0f,1.0f,
                1.0f,0.0f,1.0f,
                1.0f,0.0f,1.0f,
                1.0f,0.0f,1.0f,
                1.0f,0.0f,1.0f,

                1.0f,1.0f,0.0f,
                1.0f,1.0f,0.0f,
                1.0f,1.0f,0.0f,
                1.0f,1.0f,0.0f,
                1.0f,1.0f,0.0f,
                1.0f,1.0f,0.0f,
             };

        static float[] CubeNormals =
            {
                0.0f,0.0f,-1.0f,
                0.0f,0.0f,-1.0f,
                0.0f,0.0f,-1.0f,
                0.0f,0.0f,-1.0f,
                0.0f,0.0f,-1.0f,
                0.0f,0.0f,-1.0f,

                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,
                0.0f,0.0f,1.0f,

                -1.0f,0.0f,1.0f,
                -1.0f,0.0f,1.0f,
                -1.0f,0.0f,1.0f,
                -1.0f,0.0f,1.0f,
                -1.0f,0.0f,1.0f,
                -1.0f,0.0f,1.0f,

                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,

                0.0f,-1.0f,0.0f,
                0.0f,-1.0f,0.0f,
                0.0f,-1.0f,0.0f,
                0.0f,-1.0f,0.0f,
                0.0f,-1.0f,0.0f,
                0.0f,-1.0f,0.0f,

                0.0f,1.0f,0.0f,
                0.0f,1.0f,0.0f,
                0.0f,1.0f,0.0f,
                0.0f,1.0f,0.0f,
                0.0f,1.0f,0.0f,
                0.0f,1.0f,0.0f,
             };

        public Cube(World world) : base(world)
        {
            this.Vertices = CubeVertices;
            this.Colors = CubeColors;
            this.Normals = CubeNormals;
        }

        public override void Draw()
        {
            base.Draw(PrimitiveType.Triangles, 36);
        }
    }
}
