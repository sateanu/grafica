﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema4.Engine
{
    class RevolutionSurface
    {
        float curvePointsCount;
        float rotPointsCount;
        Vector3 color;
        Func<float, float> function;
        Vector3 rotAxis;
        Vector2 dom;
        public List<Vector3> vertices;
        public List<Vector3> colors;
        public List<Vector3> normals;
        int type = 3;
        public RevolutionSurface(float curvePointsCount,float rotPointsCount,Vector3 color,Vector3 rotAxis, Vector2 dom, Func<float, float> function)
        {
            this.curvePointsCount = curvePointsCount;
            this.rotPointsCount = rotPointsCount;
            this.color = color;
            this.function = function;
            this.rotAxis = rotAxis.Normalized();
            vertices = new List<Vector3>();
            colors = new List<Vector3>();
            normals = new List<Vector3>();
            this.dom = dom;
        }

        public void Generate()
        {
            float step = (float)Math.PI * 2 / rotPointsCount;
            if (dom.X > dom.Y)
            {
                float aux = dom.X;
                dom.X = dom.Y;
                dom.Y = aux;
            }

            float curveStep = (dom.Y - dom.X) / (curvePointsCount);

            for (int i = 0; i <= rotPointsCount; i++)
            {
                for (int j = 0; j <= curvePointsCount/2; j++)
                {
                    float xPos = j * 2 * curveStep + dom.X;
                    float xNextPos = xPos + curveStep;

                    float yPos = i * step;
                    float yNextPos = ((i + 1) % rotPointsCount) * step;

                    float res = function(xPos);
                    float resNext = function(xNextPos);
                    //Z
                    if (type == 1)
                    {
                        vertices.Add(new Vector3(res * (float)Math.Sin(yNextPos), res * (float)Math.Cos(yNextPos), xPos));
                        vertices.Add(new Vector3(res * (float)Math.Sin(yPos), res * (float)Math.Cos(yPos), xPos));
                        vertices.Add(new Vector3(res * (float)Math.Sin(yNextPos), res * (float)Math.Cos(yNextPos), xNextPos));
                        vertices.Add(new Vector3(res * (float)Math.Sin(yPos), res * (float)Math.Cos(yPos), xNextPos));
                    }
                    //X
                    if(type==2)
                    {
                        vertices.Add(new Vector3(xPos,res * (float)Math.Sin(yNextPos), res * (float)Math.Cos(yNextPos)));
                        vertices.Add(new Vector3(xPos,res * (float)Math.Sin(yPos), res * (float)Math.Cos(yPos)));
                        vertices.Add(new Vector3(xNextPos,res * (float)Math.Sin(yNextPos), res * (float)Math.Cos(yNextPos)));
                        vertices.Add(new Vector3(xNextPos,res * (float)Math.Sin(yPos), res * (float)Math.Cos(yPos)));
                    }
                    //Y
                    if(type==3)
                    {
                        vertices.Add(new Vector3(res * (float)Math.Sin(yNextPos), xPos,  res * (float)Math.Cos(yNextPos)));
                        vertices.Add(new Vector3(res * (float)Math.Sin(yPos), xPos, res * (float)Math.Cos(yPos)));
                        vertices.Add(new Vector3(res * (float)Math.Sin(yNextPos), xNextPos, res * (float)Math.Cos(yNextPos)));
                        vertices.Add(new Vector3(res * (float)Math.Sin(yPos), xNextPos, res * (float)Math.Cos(yPos)));
                    }
                }
            }

            Vector3 n1 = vertices[0];
            Vector3 n2 = vertices[1];
            normals.Add(n1);
            normals.Add(n2);

            for (int i = 0; i < vertices.Count()-2; i++)
            {
                Vector3 n3 = vertices[i+2];
                Vector3 n;
                if (i % 2 == 0)
                    n = Vector3.Normalize(Vector3.Cross(n2 - n1, n3 - n1));
                else
                    n = Vector3.Normalize(Vector3.Cross(n1 - n2, n3 - n2));

                if (n != n)
                    n = Vector3.Zero;
                normals.Add(-n);

                n1 = n2;
                n2 = n3;
            }


            for (int i = 0; i < vertices.Count; i++)
            {
                colors.Add(new Vector3((float)(vertices[i].X),
                       (float)(vertices[i].Y),
                       (float)(vertices[i].Z) + 1f));
                //colors.Add(color);
            }
            
        }

    }
}
