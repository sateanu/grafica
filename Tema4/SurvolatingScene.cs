﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

using Color = System.Drawing.Color;
using System.IO;
using System.Runtime.InteropServices;
using Tema4.Engine;
using OpenTK.Input;
using System.Diagnostics;

namespace Tema4
{
    public class SurvolatingScene : GameWindow
    {
        
        Actor actor;
        World world;
        Actor cube;
        Vase vaza;

        #region Constructors
        public SurvolatingScene()
        {

        }

        public SurvolatingScene(int width, int height) : base(width, height)
        {
        }

        public SurvolatingScene(int width, int height, GraphicsMode mode) : base(width, height, mode)
        {
        }

        public SurvolatingScene(int width, int height, GraphicsMode mode, string title) : base(width, height, mode, title)
        {
        }

        public SurvolatingScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options) : base(width, height, mode, title, options)
        {
        }

        public SurvolatingScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options, DisplayDevice device) : base(width, height, mode, title, options, device)
        {
        }

        public SurvolatingScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options, DisplayDevice device, int major, int minor, GraphicsContextFlags flags) : base(width, height, mode, title, options, device, major, minor, flags)
        {
        }

        public SurvolatingScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options, DisplayDevice device, int major, int minor, GraphicsContextFlags flags, IGraphicsContext sharedContext) : base(width, height, mode, title, options, device, major, minor, flags, sharedContext)
        {
        }
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GL.Enable(EnableCap.DepthTest);
            GL.ClearColor(Color.Black);
            GL.DepthMask(true);
            LoadShaders();
            
            world = new World();
            CursorVisible = false;
            OpenTK.Input.Mouse.SetPosition(0, 0);
            float fov = (float)Math.PI / 4;
            //world.Camera.Projection = Matrix4.Crea(0,ClientRectangle.Width, ClientRectangle.Height,0, 0.1f, 100000.0f);
            world.Camera.Projection = Matrix4.CreatePerspectiveFieldOfView(fov, (float)ClientRectangle.Width / (float)ClientRectangle.Height, 0.1f, 10000f);

            world.Camera.Position = new Vector3(0, 0, 300);
            world.Camera.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0));

            actor = new Square(world);
            actor.Initialize();

            cilinder = new Cilinder(world,new Vector3(0.5f,0,1));
            cilinder.Initialize();

            cube = new Cube(world);
            cube.Initialize();

            vaza = new Vase(world,new Vector3(1,0,0));
            vaza.Initialize();

            horn = new Horn(world);
            horn.Initialize();

            lightCube = new Cube(world);
            lightCube.Initialize();
            OpenTK.Input.Mouse.SetPosition(Bounds.Left + Bounds.Width / 2,
               Bounds.Top + Bounds.Height / 2);


        }

        private void LoadShaders()
        {
            Globals.ProgramId = GL.CreateProgram();

            int vertex = GL.CreateShader(ShaderType.VertexShader);
            int fragment = GL.CreateShader(ShaderType.FragmentShader);
            string vertexShader = File.ReadAllText(@"Shaders/light.vert");
            string fragmentShader = File.ReadAllText(@"Shaders/light.frag");

            GL.ShaderSource(vertex, vertexShader);
            GL.CompileShader(vertex);
            Console.WriteLine("Shader: " + GL.GetShaderInfoLog(vertex));

            GL.ShaderSource(fragment, fragmentShader);
            GL.CompileShader(fragment);
            Console.WriteLine("Shader: " + GL.GetShaderInfoLog(fragment));

            GL.AttachShader(Globals.ProgramId, vertex);
            GL.AttachShader(Globals.ProgramId, fragment);
            GL.LinkProgram(Globals.ProgramId);

            GL.UseProgram(Globals.ProgramId);

            GL.DeleteShader(vertex);
            GL.DeleteShader(fragment);
        }
        MouseState previousMouse;
        double elapsedTime;
        TimeSpan previous;
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            TimeSpan current = DateTime.Now.TimeOfDay;
            elapsedTime = (current - previous).TotalSeconds;
            previous = current;
            UpdateMouse();
            base.OnUpdateFrame(e);
        }

        float mouseAcceleration = 0.001f;

        private void UpdateMouse()
        {
            MouseState currentMouse = OpenTK.Input.Mouse.GetState();
            if (currentMouse != previousMouse)
            {
                world.Camera.Rotation.Y -= (currentMouse.X-previousMouse.X) * mouseAcceleration;
                world.Camera.Rotation.X -= (currentMouse.Y-previousMouse.Y) * mouseAcceleration;
            }
            previousMouse = currentMouse;
            
        }

        float x = 0;

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            x = x + 1.5f;
            
            actor.Position = new Vector3(100,-100,0);
            actor.Scale = new Vector3(1, 1, 1) * 25;
            actor.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(x), MathHelper.DegreesToRadians(0));
            actor.Draw();

            cilinder.Position = new Vector3(100, 100, 0);
            cilinder.Scale = new Vector3(1, 2, 1) * 25;
            cilinder.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(-x), MathHelper.DegreesToRadians(0));
            cilinder.Draw();

            cube.Position = new Vector3(-200, 100, 0);
            cube.Scale = new Vector3(2, 0.5f, 1)* 25;
            cube.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0));
            cube.Draw();

            lightCube.Position = Globals.lightPos;
            lightCube.Scale = new Vector3(1, 1f, 1f)*25;
            lightCube.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0));
            lightCube.Draw();

            vaza.Position = new Vector3(-100, -100, 0);
            vaza.Scale = new Vector3(1, 1, 1) * 25;
            vaza.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0));
            vaza.Draw();

            horn.Position = new Vector3(0, 250, 0);
            horn.Scale = new Vector3(1, 1, 1) * 25;
            horn.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(x*10), MathHelper.DegreesToRadians(0));
            horn.Draw();

            this.SwapBuffers();
        }

       
        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
        }
        float walkSpeed = 100;
        private Cilinder cilinder;
        private Horn horn;
        private Cube lightCube;

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            Vector3 moveVector = Vector3.Zero;
            switch (e.Key)
            {
                case Key.W:
                    moveVector.Z = -1;
                    break;
                case Key.S:
                    moveVector.Z = 1;
                    break;
                case Key.A:
                    moveVector.X = -1;
                    break;
                case Key.D:
                    moveVector.X = 1;
                    break;
                case Key.Space:
                    moveVector.Y = 1;
                    break;
                case Key.LControl:
                    moveVector.Y = -1;
                    break;
                case Key.Keypad8:
                    Globals.lightPos.Z--;
                    break;
                case Key.Keypad5:
                    Globals.lightPos.Z++;
                    break;
                case Key.Keypad4:
                    Globals.lightPos.X--;
                    break;
                case Key.Keypad6:
                    Globals.lightPos.X++;
                    break;
                case Key.Keypad7:
                    Globals.lightPos.Y++;
                    break;
                case Key.Keypad1:
                    Globals.lightPos.Y--;
                    break;
                default:
                    break;
            }
            world.Camera.AddPosition(moveVector*(float)elapsedTime*walkSpeed);
            base.OnKeyDown(e);
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    Exit();
                    break;
                default:
                    break;
            }
            base.OnKeyUp(e);
        }
    }
}
