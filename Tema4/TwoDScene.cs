﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

using Color = System.Drawing.Color;
using System.IO;
using System.Runtime.InteropServices;
using Tema4.Engine;
using OpenTK.Input;
using System.Diagnostics;

namespace Tema4
{
    public class TwoDScene : GameWindow
    {
        Actor car1;
        Actor car2;
        World world;

        #region Constructors
        public TwoDScene()
        {

        }

        public TwoDScene(int width, int height) : base(width, height)
        {
        }

        public TwoDScene(int width, int height, GraphicsMode mode) : base(width, height, mode)
        {
        }

        public TwoDScene(int width, int height, GraphicsMode mode, string title) : base(width, height, mode, title)
        {
        }

        public TwoDScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options) : base(width, height, mode, title, options)
        {
        }

        public TwoDScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options, DisplayDevice device) : base(width, height, mode, title, options, device)
        {
        }

        public TwoDScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options, DisplayDevice device, int major, int minor, GraphicsContextFlags flags) : base(width, height, mode, title, options, device, major, minor, flags)
        {
        }

        public TwoDScene(int width, int height, GraphicsMode mode, string title, GameWindowFlags options, DisplayDevice device, int major, int minor, GraphicsContextFlags flags, IGraphicsContext sharedContext) : base(width, height, mode, title, options, device, major, minor, flags, sharedContext)
        {
        }
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GL.Enable(EnableCap.DepthTest);
            GL.ClearColor(Color.Black);
            GL.DepthMask(true);
            LoadShaders();

            world = new World();
            CursorVisible = false;
            OpenTK.Input.Mouse.SetPosition(0, 0);
            float fov = (float)Math.PI / 4;
            world.Camera.Projection = Matrix4.CreateOrthographic(ClientRectangle.Width, ClientRectangle.Height, 0.1f, 100000.0f);
            //world.Camera.Projection = Matrix4.CreatePerspectiveFieldOfView(fov, (float)ClientRectangle.Width / (float)ClientRectangle.Height, 0.1f, 10000f);

            world.Camera.Position = new Vector3(0, 0, 50);
            world.Camera.Rotation = new Vector3(MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0));

            car1 = new Square(world,new Vector3(1,0,0));
            car1.Position = new Vector3(0, -ClientRectangle.Height/2, 0);
            car1.Scale = new Vector3(1, 1f, 1) * 20;
            car1.Initialize();

            car2 = new Square(world,new Vector3(0,0,1));
            car2.Position = new Vector3(0, -ClientRectangle.Height/2+200, 0);
            car2.Scale = new Vector3(1, 1f, 1) * 20;
            car2.Initialize();

            OpenTK.Input.Mouse.SetPosition(Bounds.Left + Bounds.Width / 2,
               Bounds.Top + Bounds.Height / 2);

        }

        private void LoadShaders()
        {
            Globals.ProgramId = GL.CreateProgram();

            int vertex = GL.CreateShader(ShaderType.VertexShader);
            int fragment = GL.CreateShader(ShaderType.FragmentShader);
            string vertexShader = File.ReadAllText(@"Shaders/normal.vert");
            string fragmentShader = File.ReadAllText(@"Shaders/normal.frag");

            GL.ShaderSource(vertex, vertexShader);
            GL.CompileShader(vertex);
            Console.WriteLine("Shader: " + GL.GetShaderInfoLog(vertex));

            GL.ShaderSource(fragment, fragmentShader);
            GL.CompileShader(fragment);
            Console.WriteLine("Shader: " + GL.GetShaderInfoLog(fragment));

            GL.AttachShader(Globals.ProgramId, vertex);
            GL.AttachShader(Globals.ProgramId, fragment);
            GL.LinkProgram(Globals.ProgramId);

            GL.UseProgram(Globals.ProgramId);

            GL.DeleteShader(vertex);
            GL.DeleteShader(fragment);
        }
        MouseState previousMouse;
        double elapsedTime;
        TimeSpan previous;
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            TimeSpan current = DateTime.Now.TimeOfDay;
            elapsedTime = (current - previous).TotalSeconds;
            previous = current;
            //UpdateMouse();
            if(Math.Abs(car1.Position.Y-car2.Position.Y)<75)
            {
                var xPos = car1.Position.X;
                xPos -= 1.5f;
                xPos = MathHelper.Clamp(xPos, -50, 0);
                car1.Position = new Vector3(xPos, car1.Position.Y, car1.Position.Z);
            }
            else if (Math.Abs(car1.Position.Y - car2.Position.Y) > 0)
            {
                var xPos = car1.Position.X;
                xPos += 1.5f;
                xPos = MathHelper.Clamp(xPos, -50, 0);
                car1.Position = new Vector3(xPos, car1.Position.Y, car1.Position.Z);
            }
            car1.Position = Vector3.Add(car1.Position,new Vector3(0, 2, 0));
            car2.Position = Vector3.Add(car2.Position, new Vector3(0, 1, 0));

            base.OnUpdateFrame(e);
        }

        float mouseAcceleration = 0.001f;

        private void UpdateMouse()
        {
            MouseState currentMouse = OpenTK.Input.Mouse.GetState();
            if (currentMouse != previousMouse)
            {
                world.Camera.Rotation.Y -= (currentMouse.X - previousMouse.X) * mouseAcceleration;
                world.Camera.Rotation.X -= (currentMouse.Y - previousMouse.Y) * mouseAcceleration;
            }
            previousMouse = currentMouse;

        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            car1.Draw();
            car2.Draw();

            this.SwapBuffers();
        }


        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
        }
        float walkSpeed = 100;

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            Vector3 moveVector = Vector3.Zero;
            switch (e.Key)
            {
                case Key.W:
                    moveVector.Z = -1;
                    break;
                case Key.S:
                    moveVector.Z = 1;
                    break;
                case Key.A:
                    moveVector.X = -1;
                    break;
                case Key.D:
                    moveVector.X = 1;
                    break;
                case Key.Space:
                    moveVector.Y = 1;
                    break;
                case Key.LControl:
                    moveVector.Y = -1;
                    break;
                default:
                    break;
            }
            world.Camera.AddPosition(moveVector * (float)elapsedTime * walkSpeed);
            base.OnKeyDown(e);
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    Exit();
                    break;
                default:
                    break;
            }
            base.OnKeyUp(e);
        }
    }
}
